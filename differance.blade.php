---------------------- header-lower

<div class="navbar-collapse collapse clearfix">
                    <ul class="navigation">
                        <li class="current"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="dropdown"><a href="#">About Us</a>
                            <ul class="submenu">
                                <li><a href="{{ url('about-us/idea') }}">Idea</a></li>
                                <li><a href="{{ url('about-us/themes') }}">Themes</a></li>
                                <li><a href="{{ url('about-us/geolocation') }}">Geographic area</a></li>
                                <li><a href="faq.html">Coordinators</a></li>
                                <li><a href="faq.html">Advisory Board</a></li>
                                <li><a href="{{ url('about-us/label') }}">Social Label</a></li>
                                <li><a href="{{ url('about-us/forums') }}">Forum</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="">Change Makers</a>
                            <ul class="submenu">
                                <li><a href="{{ url('/changemakers/register') }}">Registration</a></li>
                                <li><a href="single-causes.html">Benefits</a></li>
                                <li><a href="single-causes.html">Voluntary Hour Program</a></li>
                                <li><a href="{{ url('/calls') }}">Calls</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="event.html">Companies</a>
                            <ul class="submenu">
                                <li><a href="{{ url('company/register') }}">Registration</a></li>
                                <li><a href="single-event.html">Benefits</a></li>
                                <li><a href="{{ url('/offers') }}">Offers</a></li>
                                @if(Auth::check() && Auth::user()->type != 3 )
                                    <li><a href="single-event.html">Add an Offer</a></li>
                                @endif
                                @if(Auth::check() && Auth::user()->type != 3 )
                                    <li><a href="single-event.html">Add a Call</a></li>
                                @endif
                                <li><a href="single-event.html">CSR</a></li>
                                <li><a href="single-event.html">Fees</a></li>
                            </ul>
                        </li>

                        <li class="dropdown"><a href="contact.html">Partners</a>
                            <ul class="submenu from-left">
                                <li><a href="{{ url('partners/register') }}">Registration</a></li>
                                <li><a href="{{ url('partner/benefits') }}">Benefits</a></li>
                            </ul>
                        </li>

                        <li class="dropdown"><a href="{{ url('/faq') }}">FAQ</a>
                            <ul class="submenu">
                                <li class="dropdown">
                                    <a href="">Registration</a>
                                    <ul class="submenu">
                                        <li><a href="{{ url('/faq/r_m') }}">Change Makers</a></li>
                                        <li><a href="{{ url('/faq/r_c') }}">Companies</a></li>
                                        <li><a href="{{ url('/faq/r_p') }}">Partners</a></li>
                                        <li><a href="{{ url('/faq/r_o') }}">Public Organizations</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/faq/l') }}">Social Label</a></li>
                                <li><a href="{{ url('/faq/f') }}">Fees</a></li>
                                <li><a href="{{ url('/faq/d') }}">Donation</a></li>
                                <li><a href="{{ url('/faq/g') }}">general</a></li>

                            </ul>
                        </li>

                        <li class="dropdown"><a href="{{ url('/blog/posts') }}">Blog</a></li>
                    </ul>
                </div>











                ------------------------------------ header-top


                <!--Header Top-->


<div class="header-top">
    <div class="page-container reset">
        <div class="row clearfix">
            <!--Top Left-->
            <div class="col-md-6 col-sm-6 col-xs-12 top-left">
                <div class="clearfix">
                    <div class="pull-left phone-num">
                        <span class="icon flaticon-telephone51"></span>
                        Call us : <a href="#">{{ \App\SiteConfig::getValueByKey('phone') }}</a>
                    </div>
                    <div class="pull-left email">
                        <span class="icon flaticon-mail115"></span>
                        Send email :
                        <a href="{{ \App\SiteConfig::getValueByKey('email') }}">
                            {{ \App\SiteConfig::getValueByKey('email') }}
                        </a>
                    </div>
                </div>
            </div>
            <!--Top Right-->
            <div class="col-md-6 col-sm-6 col-xs-12 top-right">
                <div class="registeration">
                @if(Auth::check() === true)
                        <a href="{{ url('/logout') }}" style="margin-right:10px;">Logout</a>
                    @if(Auth::user()->type == 1)
                        <a href="{{ url('/country/account') }}">Account</a>
                    @elseif(Auth::user()->type == 2)
                        <a href="{{ url('/company/account') }}">Account</a>
                    @elseif(Auth::user()->type == 3)
                        <a href="{{ url('/changemaker/account') }}">Account</a>
                    @elseif(Auth::user()->type == 0)
                        <a href="{{ url('/panel') }}">Account</a>
                    @endif
                @else
                    <a href="{{ url('user/login') }}">login</a>
                @endif
                </div>
            </div>

        </div>
    </div>
</div>



-------------------------- routes

<?php
//|----------------------------------------------------------------|
//|                     Site Main Routes                           |
//|----------------------------------------------------------------|
Route::get('/', 'Site\HomeController@index');
Route::post('/home/contact_us', 'Site\HomeController@contactUs');
Route::get('/faq', 'Common\FaqController@homeFaq');
Route::get('/faq/{category}', 'Common\FaqController@getCategoryQuestions');
// About Us Routes

//  Ideas
Route::get('/about-us/idea', 'Common\AboutUsController@idea');
Route::get('/about-us/idea/{id}', 'Common\AboutUsController@getIdea');
//  Themes
Route::get('/about-us/themes', 'Common\AboutUsController@themes');
Route::get('/about-us/themes/{id}', 'Common\AboutUsController@getTheme');
//  Forums
Route::get('/about-us/forums', 'Common\AboutUsController@forums');
Route::get('/about-us/forums/{id}', 'Common\AboutUsController@getForum');
//  GeoLocation
Route::get('/about-us/geolocation', 'Common\AboutUsController@geoLocation');
//  Label
Route::get('/about-us/label', 'Common\AboutUsController@label');

//  Blog

//  All Posts
Route::get('blog/category/{title}', 'Common\Blog\CategoriesController@viewCategory');
Route::get('/blog/posts', 'Common\Blog\BlogController@index');
Route::get('/blog/posts/{title}', 'Common\Blog\PostsController@viewPost');

Route::get('user/login', 'Auth\LoginController@getLoginView');
Route::post('user/login', 'Auth\LoginController@validateUser');

// Calls View On Site
Route::get('/calls', 'Common\CallsController@viewCallsInSite');
Route::post('/calls/filter', 'Common\CallsController@filterCalls');
Route::get('/calls/view/{id}', 'Common\CallsController@viewCallInSite');

// Offers View On Site
Route::get('/offers', 'Common\OffersController@viewOffersInSite');
Route::get('/offers/view/{id}', 'Common\OffersController@viewSingleOfferInSite');
Route::post('/offers/filter', 'Common\OffersController@filterOffers');
Route::get('/list-offer_types', 'Common\OffersController@listOfferTypes');
Route::get('/offers/sectors', 'Common\OffersController@getSectors');

// Get Country Cities
Route::get('/countries/getcities/{id}', 'Common\CountriesController@getCities');
// List All Cities
Route::get('/list-countries', 'Common\CountriesController@listCountriesAjax');


//|----------------------------------------------------------------|
//|                     Admin Routes                               |
//|----------------------------------------------------------------|
Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/panel', 'Panel\PanelController@panelDashboard')->name('dashboard');
    Route::get('/panel/admin/settings', 'Panel\PanelController@getAdminSettings');
    Route::post('/panel/admin/settings', 'Panel\PanelController@updateAdminSettings');

    //    Site Config Routes

    Route::get('/panel/admin/site-config', 'Common\SiteConfigController@index');
    Route::post('/panel/admin/site-config', 'Common\SiteConfigController@storeConfig');

    // Sponsers Routes

    Route::get('/panel/sponsers', 'Common\SponsersController@index')->name('sponsers');
    Route::get('/panel/sponsers/new', 'Common\SponsersController@newSponser');
    Route::post('/panel/sponsers/new', 'Common\SponsersController@createSponser');
    Route::get('/panel/sponsers/{id}/edit', 'Common\SponsersController@editSponser');
    Route::post('/panel/sponsers/{id}/edit', 'Common\SponsersController@updateSponser');
    Route::post('/panel/sponsers/{id}/delete', 'Common\SponsersController@deleteSponser');

    //    Home Routes

    //    Home Slider Routes
    Route::get('panel/home/slider', 'Common\SliderController@index')->name('slider');
    Route::get('panel/home/slider/new', 'Common\SliderController@newSlide');
    Route::post('panel/home/slider/new', 'Common\SliderController@storeSlide');
    Route::get('panel/home/slider/{id}/edit', 'Common\SliderController@editSlide');
    Route::post('panel/home/slider/{id}/edit', 'Common\SliderController@updateSlide');
    Route::post('panel/home/slider/{id}/imageDelete', 'Common\SliderController@deleteSlideImage');

    //    Get all messages
    Route::get('panel/home/contact_us', 'Panel\PanelController@getContactUsMessages')->name('contactUs');
    //    Get One Message
    Route::get('panel/home/contact_us/{id}', 'Panel\PanelController@getContactUsMessage');
    //    Delete One Message
    Route::post('panel/home/contact_us/{id}/delete', 'Panel\PanelController@deleteContactUsMessage');
    //    Delete All Messages
    Route::post('panel/home/contact_us/messages/delete', 'Panel\PanelController@deleteContactUsMessages');

    //  About Us Routes

    //  Ideas
    Route::get('panel/about_us/idea', 'Panel\AboutUs\IdeasController@index')->name('ideas');
    Route::get('panel/about_us/idea/new', 'Panel\AboutUs\IdeasController@newIdea');
    Route::post('panel/about_us/idea/new', 'Panel\AboutUs\IdeasController@createIdea');
    Route::get('panel/about_us/idea/{id}', 'Panel\AboutUs\IdeasController@getIdea');
    Route::get('panel/about_us/idea/{id}/edit', 'Panel\AboutUs\IdeasController@editIdea');
    Route::post('panel/about_us/idea/{id}/edit', 'Panel\AboutUs\IdeasController@updateIdea');
    Route::post('panel/about_us/idea/{id}/delete', 'Panel\AboutUs\IdeasController@deleteIdea');

    //  Themes
    Route::get('panel/about_us/themes', 'Panel\AboutUs\ThemesController@index')->name('themes');
    Route::get('panel/about_us/themes/new', 'Panel\AboutUs\ThemesController@newTheme');
    Route::post('panel/about_us/themes/new', 'Panel\AboutUs\ThemesController@createTheme');
    Route::get('panel/about_us/themes/{id}', 'Panel\AboutUs\ThemesController@getTheme');
    Route::get('panel/about_us/themes/{id}/edit', 'Panel\AboutUs\ThemesController@editTheme');
    Route::post('panel/about_us/themes/{id}/edit', 'Panel\AboutUs\ThemesController@updateTheme');
    Route::post('panel/about_us/themes/{id}/delete', 'Panel\AboutUs\ThemesController@deleteTheme');

    //  Forums
    Route::get('panel/about_us/forums', 'Panel\AboutUs\ForumsController@index')->name('forums');
    Route::get('panel/about_us/forums/new', 'Panel\AboutUs\ForumsController@newForum');
    Route::post('panel/about_us/forums/new', 'Panel\AboutUs\ForumsController@createForum');
    Route::get('panel/about_us/forums/{id}', 'Panel\AboutUs\ForumsController@getForum');
    Route::get('panel/about_us/forums/{id}/edit', 'Panel\AboutUs\ForumsController@editForum');
    Route::post('panel/about_us/forums/{id}/edit', 'Panel\AboutUs\ForumsController@updateForum');
    Route::post('panel/about_us/forums/{id}/delete', 'Panel\AboutUs\ForumsController@deleteForum');

    // Geo Location
    Route::get('panel/about_us/geo', 'Common\AboutUsController@viewPanelGeoForm');

    // Label
    Route::get('panel/about_us/labels', 'Panel\AboutUs\LabelsController@index')->name('labels');
    Route::get('panel/about_us/labels/new', 'Panel\AboutUs\LabelsController@newLabel');
    Route::post('panel/about_us/labels/new', 'Panel\AboutUs\LabelsController@createLabel');
    Route::get('panel/about_us/labels/{id}/edit', 'Panel\AboutUs\LabelsController@editLabel');
    Route::post('panel/about_us/labels/{id}/edit', 'Panel\AboutUs\LabelsController@updateLabel');
    Route::post('panel/about_us/labels/{id}/delete', 'Panel\AboutUs\LabelsController@deleteLabel');

    //    FAQ
    Route::get('panel/faq', 'Common\FaqController@index')->name('faq');
    Route::get('panel/faq/new', 'Common\FaqController@newQuestion');
    Route::post('panel/faq/new', 'Common\FaqController@storeQuestion');
    Route::get('panel/faq/{id}/edit', 'Common\FaqController@editQuestion');
    Route::post('panel/faq/{id}/edit', 'Common\FaqController@updateQuestion');
    Route::post('panel/faq/{id}/delete', 'Common\FaqController@deleteQuestion');

    //  Blog
    //  Categories
    Route::get('panel/blog/categories', 'Common\Blog\CategoriesController@index')->name('categories');
    Route::get('panel/blog/categories/new', 'Common\Blog\CategoriesController@newCategory');
    Route::post('panel/blog/categories/new', 'Common\Blog\CategoriesController@storeCategory');
    Route::get('panel/blog/categories/{id}/edit', 'Common\Blog\CategoriesController@editCategory');
    Route::post('panel/blog/categories/{id}/edit', 'Common\Blog\CategoriesController@updateCategory');
    Route::post('panel/blog/categories/{id}/delete', 'Common\Blog\CategoriesController@deleteCategory');

    //  Posts
    Route::get('panel/blog/posts', 'Common\Blog\PostsController@index')->name('posts');
    Route::get('panel/blog/posts/new', 'Common\Blog\PostsController@newPost');
    Route::post('panel/blog/posts/new', 'Common\Blog\PostsController@storePost');
    Route::get('panel/blog/posts/{id}/edit', 'Common\Blog\PostsController@editPost');
    Route::post('panel/blog/posts/{id}/edit', 'Common\Blog\PostsController@updatePost');
    Route::post('panel/blog/posts/{id}/delete', 'Common\Blog\PostsController@deletePost');
    Route::post('panel/blog/posts/images/{id}/delete', 'Common\Blog\PostsController@deletePostImage');
    Route::get('panel/blog/category/posts/{id}', 'Common\Blog\PostsController@getCategoryPosts');


    // Countries
    Route::get('panel/countries', 'Common\CountriesController@index')->name('countries');
    Route::get('panel/countries/{letter}', 'Common\CountriesController@getCountriesByFirstLetter');
    Route::get('panel/countries/country/{id}', 'Common\CountriesController@viewCountry');
    Route::post('panel/countries/country/{id}', 'Common\CountriesController@updateCountry');

    // Citites
    Route::get('panel/cities', 'Common\CitiesController@index')->name('cities');
    Route::get('panel/cities/new', 'Common\CitiesController@newCity');
    Route::post('panel/cities/new', 'Common\CitiesController@storeCity');
    Route::get('panel/cities/edit/{id}', 'Common\CitiesController@editCity');
    Route::post('panel/cities/edit/{id}', 'Common\CitiesController@updateCity');
    Route::post('panel/cities/delete/{id}', 'Common\CitiesController@deleteCity');

    // Calls
    Route::get('panel/calls', 'Common\CallsController@adminListAllCalls')->name('panel.calls');
    Route::get('panel/calls/{id}/view', 'Common\CallsController@viewCall');
    // New Call
    Route::get('panel/calls/new', 'Common\CallsController@adminNewCall');
    Route::post('panel/calls/new', 'Common\CallsController@storeCall');
    // Edit Calls
    Route::get('panel/calls/{id}/edit', 'Common\CallsController@editCall');
    Route::post('panel/calls/{id}/edit', 'Common\CallsController@updateCall');
    // Calls Status (Accepted Or Rejected)
    Route::get('panel/calls/{id}/status/{status}', 'Common\CallsController@callStatus');
    // Calls Activation(Active Or Not)
    Route::get('panel/calls/{id}/activation/{active}', 'Common\CallsController@callActivation');
    Route::post('panel/calls/{id}/delete', 'Common\CallsController@deleteCall');
    Route::get('panel/calls/new/get-country-cities', 'Common\CountriesController@getCities');
    Route::get('panel/calls/{id}', 'Common\CallsController@getCall');

    // Offers
    Route::get('panel/offers/', 'Common\OffersController@index')->name('panel.offers');
    Route::get('panel/offers/{id}/view', 'Common\OffersController@viewOffer');
    Route::get('panel/offers/new', 'Common\OffersController@adminNewOffer');
    Route::post('panel/offers/new', 'Common\OffersController@storeOffer');

    Route::get('panel/offers/{id}/edit', 'Common\OffersController@editOffer');
    Route::post('panel/offers/{id}/edit', 'Common\OffersController@updateOffer');
    Route::post('panel/offers/{id}/delete', 'Common\OffersController@deleteOffer');

    // Offer Status (Accepted Or Rejected)
    Route::get('panel/offers/{id}/status/{status}', 'Common\OffersController@offerStatus');
    // Offer Activation(Active Or Not)
    Route::get('panel/offers/{id}/activation/{active}', 'Common\OffersController@offerActivation');

});

//|----------------------------------------------------------------|
//|                     Country Routes                             |
//|----------------------------------------------------------------|
Route::group(['middleware' => ['country']], function () {
    Route::get('/country/account', 'Common\CountriesController@countryAccount');
    Route::post('account/update/{id}', 'Common\CountriesController@updateCountry');
    // Calls

    // New Call
    Route::get('/country/calls', 'Common\CountriesController@viewAllCalls')->name('country.calls');
    Route::get('/country/calls/new', 'Common\CountriesController@newCall');
    //  Post To Calls Controller
    Route::post('/country/calls/new', 'Common\CallsController@storeCall');
});

//|----------------------------------------------------------------|
//|                     ChangeMaker Routes                         |
//|----------------------------------------------------------------|
// Change Makers
// Registers
Route::get('/changemakers/register', 'Common\ChangemakersController@newChangemaker');
Route::post('/changemakers/register', 'Common\ChangemakersController@storeChangemaker');
Route::group(['middleware' => ['changemaker']], function () {
    Route::get('/changemaker/account', 'Common\ChangemakersController@changeMakerAccount')->name('changemaker');
    Route::post('/changemaker/account', 'Common\ChangemakersController@updateChangeMakerAccount');
    Route::post('changemaker/account/check-user-password', 'Common\ChangemakersController@updatePassword');
    Route::post('changemaker/account/change_password', 'Common\ChangemakersController@updatePassword');
});


//|----------------------------------------------------------------|
//|                     Company Routes                             |
//|----------------------------------------------------------------|
// Company Register
Route::get('company/register', 'Common\CompanyController@register');
Route::post('company/register', 'Common\CompanyController@storeCompany');
Route::group(['middleware' => ['company']], function () {
    Route::get('company/account', 'Common\CompanyController@companyAccount')->name('company');
    Route::post('company/account', 'Common\CompanyController@updateCompany');
    Route::get('company/settings', 'Common\CompanyController@updateCompany');
    Route::post('company/settings/check-user-password/', 'Common\CompanyController@validatePassword');
    Route::post('company/settings/change-password', 'Common\CompanyController@changePassword');

    // Calls
    Route::get('company/calls', 'Common\CompanyController@viewCalls');
    Route::get('company/calls/new', 'Common\CompanyController@newCall');
    Route::get('calls/new/get-country-cities', 'Common\CountriesController@getCities');
    Route::post('company/calls/new', 'Common\CallsController@storeCall');
//    filter
    Route::get('company/calls/filter/{type}', 'Common\CallsController@callsOwnerFilter');

});


//|----------------------------------------------------------------|
//|                     partners Routes                            |
//|----------------------------------------------------------------|
// Company Register
Route::get('/partners/register', 'Common\PartnersController@registerPartner');

// Auth Routes
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');