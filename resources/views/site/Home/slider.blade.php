<section class="main-slider style-two default-banner">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>

                @if(isset($slides))
                    @foreach($slides as $slide)
                        @if(!empty($slide->red_button_title) || !empty($slide->transparent_button_title))
                            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1000"
                                data-thumb="{{ asset('media/slider/'.$slide->image_name) }}" data-saveperformance="off"
                                data-title="{{ $slide->title }}">
                                <img src="{{ asset('media/slider/'.$slide->image_name) }}" alt=""
                                     data-bgposition="center top"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat">

                                <div class="tp-caption lfl tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-24"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="title with-bg"><h2>{{ $slide->title }}</h2></div>
                                </div>

                                <div class="tp-caption lfr tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="42"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="text with-bg"><h4>{{ $slide->caption }}</h4>
                                    </div>
                                </div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="-80"
                                     data-y="center" data-voffset="110"
                                     data-speed="1500"
                                     data-start="1000"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="link-btn">
                                        <a href="{{ url($slide->transparent_button_url) }}"
                                           class="theme-btn light-btn">
                                            {{$slide->transparent_button_title }}
                                        </a>
                                    </div>
                                </div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="80"
                                     data-y="center" data-voffset="110"
                                     data-speed="1500"
                                     data-start="1000"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="link-btn">
                                        <a href="{{ $slide->red_button_url }}" class="theme-btn dark-btn">
                                            {{$slide->red_button_title}}
                                        </a>
                                    </div>
                                </div>


                            </li>
                        @else
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000"
                                data-thumb="{{ asset($slide->image) }}" data-saveperformance="off"
                                data-title="Donation is Better">
                                <img src="{{ asset($slide->image) }}" alt="" data-bgposition="center top"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat">

                                <div class="tp-caption lft tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-24"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="title"><h2>{{ $slide->title }}</h2></div>
                                </div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="32"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="text"><h4> {{ $slide->caption }} </h4></div>
                                </div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="70"
                                     data-speed="1500"
                                     data-start="1000"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <div class="line"></div>
                                </div>

                            </li>
                        @endif
                    @endforeach
                @endif

            </ul>

        </div>
    </div>
</section>

<section class="tri-column-fluid">
    <div class="tri-column-outer clearfix">

        <!--Column-->
        <article class="column default-column">
            <div class="pattern-layer"></div>

            <div class="column-inner">
                <h3>Help 10140 people for education</h3>
                <h4>No one has ever become poor by giving</h4>
                <div class="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                    invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </div>
                <div class="text-right link"><a href="#" class="theme-btn btn-style-one">BECOME A VOLUNTEER</a></div>
            </div>
        </article>

        <!--Carousel Column-->
        <article class="column carousel-column"
                 style="background-image:url({{ asset('images/background/carousel-bg.jpg') }});">
            <div class="pattern-layer"></div>

            <div class="column-inner">
                <h3>People need your help every day.</h3>
                <h4>No one has ever become poor by giving</h4>
                <div class="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                    invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </div>
                <div class="text-right link"><a href="#" class="theme-btn btn-style-one">Make a Donation</a></div>
            </div>

        </article>

        <!--Progress Column-->
        <article class="column progress-column">
            <div class="pattern-layer"></div>

            <div class="column-inner">
                <h3>Our company served over</h3>
                <h2>24121012</h2>
                <h3 class="museo-font">children in 32 countries</h3>
                <br><br>

                <div class="text-right link"><a href="{{ url('/country') }}" class="theme-btn btn-style-two">Login As
                        Country</a></div>
            </div>
        </article>

    </div>
</section>