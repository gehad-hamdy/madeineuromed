<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Jorenvh\Share\Share;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    /**
     * @param $link
     * @param $title
     * @return Share
     */
    public function facebookShare($link, $title)
    {
        $share = new Share();
        return $share->page('http://www.example.com', 'My example')->facebook();

    }

    /**
     * @param $link
     * @param $title
     * @return Share
     */
    public function twitterShare($link, $title)
    {
        return Share::load($link, 'Your share text can be placed here','twitter');
    }

    /**
     * @param $link
     * @param $title
     * @return Share
     */
    public function linkedInShare($link, $title)
    {
        return Share::load($link, $title,'linkedin');
    }

    /**
     * @param $link
     * @param $title
     * @return Share
     */
    public function gplusInShare($link, $title)
    {
        return Share::load($link, $title,'googlePlus');
    }
}
