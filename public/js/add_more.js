$(document).ready(function () {
    var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.more_contact').append(`<hr><div>
                        <div class="form-group"><div class="col-xs-12 col-md-8"><div class="form-input" id="contact_type"><label class="radio-inline">
                        <input type="radio" name="contact_person[${x}][contact_type]" value="0"> Mr./</label><label class="radio-inline">
                        <input type="radio" name="contact_person[${x}][contact_type]" value="1">Mrs./</label><label class="radio-inline">
                        <input type="radio" name="contact_person[${x}][contact_type]" value="2"> Ms./</label><label class="radio-inline">
                        <input type="radio" name="contact_person[${x}][contact_type]" value="3"> Dr./</label>
                        <div class="form-group"><div class="col-xs-12 col-md-6"><label for="first_name"> First Name * </label>
                        <input type="text" name="contact_person[${x}][first_name]" class="form-control" id="first_name_${x}"></div>
                        <div class="col-xs-12 col-md-6"><label for="last_name"> Last Name * </label>
                        <input type="text" name="contact_person[${x}][last_name]" class="form-control" id="last_name_${x}">
                        </div></div>
                        <div class="form-group"><div class="col-xs-12 col-md-6"><label for="position"> Position </label>
                        <input type="text" name="contact_person[${x}][position]" class="form-control" id="position_${x}"></div> <div class="col-xs-12 col-md-6">
                        <label for="dep_name"> Department </label><input type="text" name="contact_person[${x}][dept_name]" class="form-control" id="dept_name_${x}">
                        </div></div>
                        <div class="form-group"><div class="col-xs-12 col-md-6"><label for="email"> Email * </label>
                        <input type="text" name="contact_person[${x}][email]" class="form-control" id="email_${x}">
                        </div></div>
                        <div class="form-group"> <div class="col-xs-12 col-md-4"><label for="tel"> Tel </label>
                        <input type="text" name="contact_person[${x}][tel]" class="form-control" id="tel_${x}">
                        </div><div class="col-xs-12 col-md-4"><label for="tax"> Tax </label>
                        <input type="text" name="contact_person[${x}][ext]" class="form-control" id="ext_${x}">
                        </div><div class="col-xs-12 col-md-4"><label for="mobile"> Mobile </label>
                        <input type="text" name="contact_person[${x}][mobile]" class="form-control" id="mobile_${x}">
                        </div>
                     </div>

                      </label></div></div></div><a href="#" class="remove_field btn btn-danger" style="margin-left:10px;">Remove</a>
                      </div>
                    `) //add input field
        }
    });
    $('.more_contact').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })

    var y = 1;
    $('.add_more_phones').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        if (y < max_fields_limit) { //check conditions
            y++; //counter increment
            $('.more_contact_phones').append(`<hr>
                 <div>
                  <div class="form-group">
                    <div class="col-xs-12 col-md-4">
                        <label for="tel"> Tel </label>
                        <input type="text" name="contact_phone[${y}][tel]" class="form-control" id="tel_${y}">
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <label for="ext"> Ext </label>
                        <input type="text" name="contact_phone[${y}][ext]" class="form-control" id="ext_${y}">
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <label for="phone"> Mobile Phone </label>
                        <input type="text" name="contact_phone[${y}][phone]" class="form-control" id="phone_${y}">
                    </div> 
                    <div class="col-xs-12 col-md-4">
                        <label for="fax"> Fax </label>
                        <input type="text" name="contact_phone[${y}][fax]" class="form-control" id="fax_${y}">
                    </div>
                   </div>
                      <a href="#" class="remove_field btn btn-danger" style="margin-left:10px;">Remove</a>
                      </div>
                    `) //add input field
        }
    });
    $('.more_contact_phones').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        $(this).parent('div').remove();
        y--;
    })

    var z = 1;
    $('.add_more').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        if (z < max_fields_limit) { //check conditions
            z++; //counter increment
            $('.add_more_doc').append(
                ` <div class="form-group">  <div class="col-xs-12 col-md-6">
                    <label for="other_doc"> Other document </label>
                    <input type="file" name="other_doc" class="form-control" id="other_doc_${z}">
                    <small> e.i cover letter, rigistration ducoments</small>
                </div></div>`) //add input field
        }
    });
});
