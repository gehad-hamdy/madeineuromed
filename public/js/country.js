
$(document).ready(function () {
    $('select').select2({
        tags: true
    });
    $('.agree').on('click', function (e) {
        e.preventDefault();
    })
    $('input.timepicker').timepicker({});

    $("#inputState").change(function () {
        if ($(this).val() === '2')
            $('#other_sector').show();
        else
            $('#other_sector').hide();
    });
    $("#selected_by").change(function () {
        if ($(this).val() === '2')
            $('#other_selected_by').show();
        else
            $('#other_selected_by').hide();
    });
    $("#address").change(function () {
        if ($(this).val() === '5')
            $('#other_address').show();
        else
            $('#other_address').hide();
    });
    $("#org_type").change(function () {
        if ($(this).val() === '2')
            $('#other_org_type').show();
        else
            $('#other_org_type').hide();
    });

    $('#no').change(function () {
        $('#organize_social_no').show();
        $('#organize_social_yes').hide();
    });
    $('#yes').change(function () {
        $('#organize_social_yes').show();
        $('#organize_social_no').hide();
    });

});